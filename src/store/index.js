import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		fireUser: null,
		user: null,
	},
	mutations: {
		setFireUser(state, fu) {
			state.fireUser = fu;
		},
		setUser(state, user) {
			state.user = user;
		},
	},
	actions: {},
	modules: {},
});
