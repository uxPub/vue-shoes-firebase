# vue-shoes-firebase

vue-shoes-firebase

# install
## firebase use
```bash
$ firebase login
$ firebase use -add
? Which project to you want to add? xxx-site
? Which alias do you want to use for this project? default

Creates alias default for xxx-site
Now using alias default (xxx-site)
```

## functions
### key download
functions/key.json download from console service account key

### setting
```bash
$ firebase functions:config:set admin.email=ux.pub2020@gmail.com admin.db_url=https://vue-shoes-c98e4-default-rtdb.firebaseio.com/  admin.region=asia-northeast1
$ firebase functions:config:get // 어드민 계정 확인
```

### deploy
```bash
$ firebase deploy // deplay all
$ yarn deploy // deploy (firebase deploy @package.json)
$ yarn deploy:fs // functions only (firebase deploy --only functions @package.json)
```

### CORS
```bash
1. https://cloud.google.com/sdk/docs/quickstart?authuser=0
curl https://sdk.cloud.google.com | bash

  Modify profile to update your $PATH and enable shell command 
  completion?

  1.1. Do you want to continue (Y/n)?  y

  The Google Cloud SDK installer will now prompt you to update an rc 
  file to bring the Google Cloud CLIs into your environment.

  Enter a path to an rc file to update, or leave blank to use 
  1.2. [/Users/arikit/.zshrc]:  /Users/arikit/vue-shoes-firebase


1.3. gsutil cors set cors.json gs://vue-shoes-c98e4.appspot.com 
```

### Editor
```bash
https://github.com/nhn/toast-ui.vue-editor
npm install --save @toast-ui/vue-editor
```